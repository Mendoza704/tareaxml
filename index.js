
function abrirXML(estudiantes){
    var docXML=undefined;
    try{
        if(document.all){
            docXML= new ActiveXObject("Microsoft.XMLDOM");
        }else{
            docXML=document.implementation.createDocument("","",null);
        }
        docXML.async=false;
        docXML.load(estudiantes);
    }catch(e){
        try{
            var httpxml= new window.XMLHttpRequest();
            httpxml.open("GET", "estudiantes.xml",false);
            httpxml.send(null);
            docXML= httpxml.responseXML.documentElement;
            return docXML;
        }catch(e){
            return docXML;
        }
    }
    return undefined;
}
docXML=abrirXML("estudiantes.xml");
var factura =docXML.getElementsByTagName("CD");
var tabla ="<thead><tr><th>Apellido</th><th>Nombre</th><th>Semestre</th><th>Paralelo</th><th>Dirección</th><th>Teléfono</th><th>Correo</th></tr></thead>"
for(i=0; i<factura.length; i++){
    tabla+="<tr><td>";
    tabla+= factura[i].getElementsByTagName("apellido")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("nombre")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("semestre")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("paralelo")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("direccion")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("telefono")[0].textContent;
    tabla+="<td>";
    tabla+= factura[i].getElementsByTagName("correo")[0].textContent;
}
document.getElementById("demo").innerHTML= tabla;